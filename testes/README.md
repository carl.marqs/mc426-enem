## Testes
Para os testes, utilizamos a ferramenta Selenium, uma extensão que pode ser instalada no Chrome para fazer o teste de elementos visuais HTML e que nos foi muito útil.

Assim, começamos os testes olhando para o preenchimento de checkboxes na parte de "Começar o Teste". Fizemos, então, um Particionamento em classes de equivalência com as possibilidades de Erro e de Sucesso ao assinalar uma Questão, resultando na tabela abaixo:

![1](testes/classe_equivalencia.jpg)

Os arquivos referentes à esta implementação podem também serem vistos nesta pasta.

Ainda com relação ao preenchimento de checkboxes, percebemos que havia também o caso de que UMA questão sozinha poderia fazer com que o questionário TODO ficasse INVÁLIDO, e, portanto, o programa encontraria um erro. Para testar isto, portanto, fizemos uma Tabela de Decisões, onde cada Questão do Questionário poderia afetar o output desta parte inteira. Pode-se ver pela imagem abaixo:

![2](testes/tabela_decisao_v2.jpg)

O código do Selenium pode ser visto nos arquivos:
<br>Caso Válido: [valid_answer](https://gitlab.com/carl.marqs/mc426-enem/-/blob/feature/A6/testes/invalid_answer.side)
<br>Caso Inválido: [invalid_answer](https://gitlab.com/carl.marqs/mc426-enem/-/blob/feature/A6/testes/valid_answer.side)

Também fizemos um teste automatizado para verificar se, ao pressionarmos os botões "Análise", "Fazer o Teste" e "Quem Somos", o site carrega a página correta referente ao título do Botão. 

![3](testes/tabela_decisao_2.jpg)

Verifica-se o teste atraves do arquivo [teste 2](https://gitlab.com/carl.marqs/mc426-enem/-/blob/feature/A6/testes/teste_2/Teste_Paginas.side), novamente, usando-se a extensão do Selenium adicionada ao Browser.

Foi implementado também, um grande sistema de teste de nossa rede neural através do Issue #21, este que será o responsável por nos dar a predição da nota final.
